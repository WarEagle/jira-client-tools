import * as JiraApi from "jira-client";

declare namespace JiraTypes {
  export interface IJiraIssueStatus {
    self: string;
    description: string;
    iconUrl: string;
    name: string;
    id: string;
    statusCategory: {
      self: string;
      id: number;
      key: string;
      colorName: string;
      name: string;
    };
  }

  export interface IJiraFields {
    status: IJiraIssueStatus;
    summary: string;
    assignee: IJiraUser;
    subtasks: IJiraIssue[];
    aggregatetimeoriginalestimate: number;
    aggregatetimespent: number;
    issuetype: IJiraIssueType;
    worklog: IJiraWorklogs;
  }

  export interface IJiraWorklogs {
    startAt: number;
    maxResults: number;
    total: number;
    worklogs: IJiraWorklog[];
  }

  export interface IJiraWorklog {
    self: string;
    author: IJiraUser;
    updateAuthor: IJiraUser;
    comment: string;
    created: string;
    updated: string;
    started: string;
    timeSpent: string;
    timeSpentSeconds: number;
    id: string;
    issueId: string;
  }

  export interface IJiraIssue {
    id: string;
    self: string;
    key: string;
    expand: string;
    fields: JiraApi.FieldObject | IJiraFields;
  }

  export interface IJiraIssueType {
    avatarId: number;
    description: string;
    iconUrl: string;
    id: string;
    name: string;
    self: string;
    subtask: boolean;
  }

  export interface IJiraBoard {
    id: number;
    name: string;
    self: string;
    type: string;
  }

  export interface IJiraUser {
    self: string;
    name: string;
    key: string;
    emailAddress: string;
    avatarUrls: {
      "48x48": string;
      "24x24": string;
      "16x16": string;
      "32x32": string;
    };
    displayName: string;
    active: boolean;
    timeZone: string;
  }

  export interface IJiraSprint {
    id: number;
    linkedPagesCount: number;
    name: string;
    sequence: number;
    state: string;
  }

  export interface DevSummaryJson {
    cachedValue: {
      errors: [],
      configErrors: [],
      summary: {
        pullrequest: {
          overall: {
            count: number,
            lastUpdated: any,
            stateCount: number,
            state: string,
            open: boolean
          },
          byInstanceType: {}
        },
        build: {
          overall: {
            count: number,
            lastUpdated: any,
            failedBuildCount: number,
            successfulBuildCount: number,
            unknownBuildCount: number
          },
          byInstanceType: {}
        },
        review: {
          overall: {
            count: number,
            lastUpdated: any,
            stateCount: number,
            state: any,
            dueDate: any,
            overDue: boolean,
            completed: boolean
          },
          byInstanceType: {}
        },
        "deployment-environment": {
          overall: {
            count: number,
            lastUpdated: any,
            topEnvironments: [],
            showProjects: boolean,
            successfulCount: number
          },
          byInstanceType: {}
        },
        repository: {
          overall: {
            count: number,
            lastUpdated: any
          },
          byInstanceType: {}
        },
        branch: {
          overall: {
            count: number,
            lastUpdated: any
          },
          byInstanceType: {}
        }
      }
    },
    isStale: boolean
  }

  export interface JiraRestResponse {
    "name": string,
    "statusCode": number,
    "message": string,
    "error": string,
    "options": {
      "auth": {
        "user": string,
        "pass": string
      },
      "rejectUnauthorized": boolean,
      "method": string,
      "uri": string,
      "json": boolean,
      "simple": boolean,
      "resolveWithFullResponse": boolean,
      "transform2xxOnly": boolean
    },
    "response": {
      "statusCode": number,
      "body": string,
      "headers": {
        "date": string,
        "server": string,
        "x-arequestid": string,
        "x-xss-protection": string,
        "x-content-type-options": string,
        "x-frame-options": string,
        "content-security-policy": string,
        "x-asen": string,
        "x-seraph-loginreason": string,
        "www-authenticate": string,
        "content-type": string,
        "connection": string,
        "transfer-encoding": string
      },
      "request": {
        "uri": {
          "protocol": string,
          "slashes": boolean,
          "auth": any,
          "host": string,
          "port": string,
          "hostname": string,
          "hash": string,
          "search": string,
          "query": string,
          "pathname": string,
          "path": string,
          "href": string
        },
        "method": string,
        "headers": {
          "authorization": string,
          "accept": string
        }
      }
    }
  }

}

