/// <reference types="node" />

import * as request from 'request';
import { ReadStream } from 'fs';

declare class JiraApi {
    private protocol: string;
    private host: string;
    private port: string | null;
    private apiVersion: string;
    private base: string;
    private intermediatePath?: string;
    private strictSSL: boolean;
    private webhookVersion: string;
    private greenhopperVersion: string;

    constructor (options: JiraApi.JiraApiOptions);

    public findIssue (issueNumber: string, expand?: string, fields?: string, properties?: string, fieldsByKeys?: boolean): Promise<JiraApi.JsonResponse>;

    public getUnresolvedIssueCount (version: string): Promise<number>;

    public getProject (project: string): Promise<JiraApi.JsonResponse>;

    public createProject (project: string): Promise<JiraApi.JsonResponse>;

    public findRapidView (projectName: string): Promise<JiraApi.JsonResponse[]>;

    public getLastSprintForRapidView (rapidViewId: string): Promise<JiraApi.JsonResponse>;

    public getSprintIssues (rapidViewId: string, sprintId: string): Promise<JiraApi.JsonResponse>;

    public listSprints (rapidViewId: string): Promise<JiraApi.JsonResponse>;

    public addIssueToSprint (issueId: string, sprintId: string): Promise<JiraApi.JsonResponse>;

    public issueLink (link: JiraApi.LinkObject): Promise<JiraApi.JsonResponse>;

    public getRemoteLinks (issueNumber: string): Promise<JiraApi.JsonResponse>;

    public createRemoteLink (issueNumber: string, remoteLink: JiraApi.LinkObject): Promise<JiraApi.JsonResponse>;

    public getVersions (project: string): Promise<JiraApi.JsonResponse>;

    public createVersion (version: string): Promise<JiraApi.JsonResponse>;

    public updateVersion (version: string): Promise<JiraApi.JsonResponse>;

    public deleteVersion (versionId: string, moveFixIssuesToId: string, moveAffectedIssuesToId: string): Promise<JiraApi.JsonResponse>;

    public searchJira (searchString: string, optional?: JiraApi.SearchQuery): Promise<JiraApi.JsonResponse>;

    public createUser (user: JiraApi.UserObject): Promise<JiraApi.JsonResponse>;

    public searchUsers (options: JiraApi.SearchUserOptions): Promise<JiraApi.JsonResponse>;

    public getUsersInGroup (groupname: string, startAt?: number, maxResults?: number): Promise<JiraApi.JsonResponse>;

    public addNewIssue (issue: JiraApi.IssueObject): Promise<JiraApi.JsonResponse>;

    public addWatcher (issueKey: string, username: string): Promise<JiraApi.JsonResponse>;

    public deleteIssue (issueId: string): Promise<JiraApi.JsonResponse>;

    public updateIssue (issueId: string, issueUpdate: JiraApi.IssueObject): Promise<JiraApi.JsonResponse>;

    public listComponents (project: string): Promise<JiraApi.JsonResponse>;

    public addNewComponent (component: JiraApi.ComponentObject): Promise<JiraApi.JsonResponse>;

    public deleteComponent (componentId: string): Promise<JiraApi.JsonResponse>;

    public createCustomField (field: JiraApi.FieldObject): Promise<JiraApi.JsonResponse>;

    public listFields (): Promise<JiraApi.JsonResponse>;

    public createFieldOption (fieldKey: string, option: JiraApi.FieldOptionObject): Promise<JiraApi.JsonResponse>;

    public listFieldOptions (fieldKey: string): Promise<JiraApi.JsonResponse>;

    public upsertFieldOption (fieldKey: string, optionId: string, option: JiraApi.FieldOptionObject): Promise<JiraApi.JsonResponse>;

    public getFieldOption (fieldKey: string, optionId: string): Promise<JiraApi.JsonResponse>;

    public deleteFieldOption (fieldKey: string, optionId: string): Promise<JiraApi.JsonResponse>;

    public getIssueProperty (issueNumber: string, property: string): Promise<JiraApi.JsonResponse>;

    public listPriorities (): Promise<JiraApi.JsonResponse>;

    public listTransitions (issueId: string): Promise<JiraApi.JsonResponse>;

    public transitionIssue (issueId: string, issueTransition: JiraApi.TransitionObject): Promise<JiraApi.JsonResponse>;

    public listProjects (): Promise<JiraApi.JsonResponse>;

    public addComment (issueId: string, comment: string): Promise<JiraApi.JsonResponse>;

    public updateComment (issueId: string, commentId: string, comment: string, options?: any): Promise<JiraApi.JsonResponse>;

    public getIssueWorklog (issueId: string): Promise<JiraApi.JsonResponse>;

    public addWorklog (issueId: string, worklog: JiraApi.WorklogObject, newEstimate: JiraApi.EstimateObject): Promise<JiraApi.JsonResponse>;

    public deleteWorklog (issueId: string, worklogId: string): Promise<JiraApi.JsonResponse>;

    public listIssueTypes (): Promise<JiraApi.JsonResponse>;

    public registerWebhook (webhook: JiraApi.WebhookObject): Promise<JiraApi.JsonResponse>;

    public listWebhooks (): Promise<JiraApi.JsonResponse>;

    public getWebhook (webhookID: string): Promise<JiraApi.JsonResponse>;

    public deleteWebhook (webhookID: string): Promise<JiraApi.JsonResponse>;

    public getCurrentUser (): Promise<JiraApi.JsonResponse>;

    public getBacklogForRapidView (rapidViewId: string): Promise<JiraApi.JsonResponse>;

    public addAttachmentOnIssue (issueId: string, readStream: ReadStream): Promise<JiraApi.JsonResponse>;

    public issueNotify (issueId: string, notificationBody: JiraApi.NotificationObject): Promise<JiraApi.JsonResponse>;

    public listStatus (): Promise<JiraApi.JsonResponse>;

    public getDevStatusSummary (issueId: string): Promise<JiraApi.JsonResponse>;

    public getDevStatusDetail (issueId: string, applicationType: string, dataType: string): Promise<JiraApi.JsonResponse>;

    public moveToBacklog (issues: string[]): Promise<JiraApi.JsonResponse>;

    public getAllBoards (startAt?: number, maxResults?: number, type?: string, name?: string, projectKeyOrId?: string): Promise<JiraApi.JsonResponse>;

    public createBoard (boardBody: JiraApi.BoardObject): Promise<JiraApi.JsonResponse>;

    public getBoard (boardId: string): Promise<JiraApi.JsonResponse>;

    public deleteBoard (boardId: string): Promise<JiraApi.JsonResponse>;

    public getIssuesForBacklog (boardId: string, startAt?: number, maxResults?: number, jql?: string, validateQuery?: boolean, fields?: string): Promise<JiraApi.JsonResponse>;

    public getConfiguration (boardId: string): Promise<JiraApi.JsonResponse>;

    public getIssuesForBoard (boardId: string, startAt?: number, maxResults?: number, jql?: string, validateQuery?: boolean, fields?: string): Promise<JiraApi.JsonResponse>;

    public getEpics (boardId: string, startAt?: number, maxResults?: number, done?: 'true' | 'false'): Promise<JiraApi.JsonResponse>;

    public getBoardIssuesForEpic (boardId: string, epicId: string, startAt?: number, maxResults?: number, jql?: string, validateQuery?: boolean, fields?: string): Promise<JiraApi.JsonResponse>;

    public getProjects (boardId: string, startAt?: number, maxResults?: number): Promise<JiraApi.JsonResponse>;

    public getProjectsFull (boardId: string): Promise<JiraApi.JsonResponse>;

    public getBoardPropertiesKeys (boardId: string): Promise<JiraApi.JsonResponse>;

    public deleteBoardProperty (boardId: string, propertyKey: string): Promise<JiraApi.JsonResponse>;

    public setBoardProperty (boardId: string, propertyKey: string, body: string): Promise<JiraApi.JsonResponse>;

    public getBoardProperty (boardId: string, propertyKey: string): Promise<JiraApi.JsonResponse>;

    public getAllSprints (boardId: string, startAt?: number, maxResults?: number, state?: 'future' | 'active' | 'closed'): Promise<JiraApi.JsonResponse>;

    public getBoardIssuesForSprint (boardId: string, sprintId: string, startAt?: number, maxResults?: number, jql?: string, validateQuery?: boolean, fields?: string): Promise<JiraApi.JsonResponse>;

    public getAllVersions (boardId: string, startAt?: number, maxResults?: number, released?: 'true' | 'false'): Promise<JiraApi.JsonResponse>;

    private makeRequestHeader (uri: string, options?: JiraApi.UriOptions);

    private makeUri (options: JiraApi.UriOptions): string;

    private makeWebhookUri (options: JiraApi.UriOptions): string;

    private makeSprintQueryUri (options: JiraApi.UriOptions): string;

    private makeDevStatusUri (options: JiraApi.UriOptions): string;

    private makeAgileUri (options: JiraApi.UriOptions): string;

    private doRequest (requestOptions: request.CoreOptions): Promise<request.RequestResponse>;
}

declare namespace JiraApi {
    interface JiraApiOptions {
        protocol?: string;
        host: string;
        port?: string;
        username?: string;
        password?: string;
        base?: string;
        intermediatePath?: string;
        strictSSL?: boolean;
        //request: Function;
        timeout?: number;
        webhookVersion?: string;
        greenhopperVersion?: string;
        apiVersion?: string;
        bearer?: string;
        oauth?: OAuth;
    }

    interface OAuth {
        consumer_key: string;
        consumer_secret: string;
        access_token: string;
        access_token_secret: string;
        signature_method?: string;
    }

    interface LinkObject {
        [name: string]: any;
    }

    interface Query {
        [name: string]: any;
    }

    interface JsonResponse {
        [name: string]: any;
    }

    interface UserObject {
        [name: string]: any;
    }

    interface IssueObject {
        [name: string]: any;
    }

    interface ComponentObject {
        [name: string]: any;
    }

    interface FieldObject {
        [name: string]: any;
    }

    interface FieldOptionObject {
        [name: string]: any;
    }

    interface TransitionObject {
        [name: string]: any;
    }

    interface WorklogObject {
        [name: string]: any;
    }

    interface EstimateObject {
        [name: string]: any;
    }

    interface WebhookObject {
        [name: string]: any;
    }

    interface NotificationObject {
        [name: string]: any;
    }

    interface BoardObject {
        [name: string]: any;
    }

    interface SearchUserOptions {
        username: string;
        startAt?: number;
        maxResults?: number;
        includeActive?: boolean;
        includeInactive?: boolean;
    }

    interface SearchQuery {
        startAt?: number;
        maxResults?: number;
        fields?: string[];
    }

    interface SearchQuery {
        startAt?: number;
        maxResults?: number;
        fields?: string[];
    }

    interface UriOptions {
        pathname: string;
        query?: Query;
        intermediatePath?: string;
    }
}

//export = JiraApi;
//export default;
