import * as JiraApi from "jira-client";
import {isNullOrUndefined} from "util";
import DevSummaryJson = JiraApi.DevSummaryJson;
import IJiraIssue = JiraApi.IJiraIssue;
import IJiraIssueStatus = JiraApi.IJiraIssueStatus;

export module JiraClientTools {

  export const CUSTOMFIELD_STORYPOINTS_ESTIMATE = 'customfield_40892';
  export const CUSTOMFIELD_STORYPOINTS_SPENT = 'customfield_46095';
  export const CUSTOMFIELD_SPRINTS = 'customfield_41780';

  export class IJiraIssueStatusName {
    public static ALL: IJiraIssueStatusName[] = [];
    public static INCOMING = new IJiraIssueStatusName("Incoming", 0);
    public static OPEN = new IJiraIssueStatusName("Open", 1);
    public static REOPENED = new IJiraIssueStatusName("Reopened", 2);
    public static IN_PROGRESS = new IJiraIssueStatusName("In Progress", 3);
    public static REVIEW = new IJiraIssueStatusName("Review", 4);
    public static MERGE = new IJiraIssueStatusName("Merge", 5);
    public static STABLE_REVIEW = new IJiraIssueStatusName("Stable Review", 6);
    public static RESOLVED = new IJiraIssueStatusName("Resolved", 7);

    public static fromString(name: string): IJiraIssueStatusName {
      for (const key of this.ALL) {
        if (key.name === name) {
          return key;
        }
      }

      console.error("Unknown status " + name);

      return undefined;
    }

    private constructor(public readonly name: string, public readonly order: number) {
      IJiraIssueStatusName.ALL.push(this);
    }

  }

  export const extractAndParseDevSummaryJson = function (inputString: string): DevSummaryJson {
    const workingString = inputString.trim();
    const start = workingString.indexOf('devSummaryJson');
    if (start === -1) {
      return undefined;
    }

    let tmp = workingString.substr(start + "devSummaryJson".length + 1);
    tmp = tmp.substr(0, tmp.length - 1);
    // console.log(tmp);
    return JSON.parse(tmp);
  };

  export const extractStatus = function (issue: IJiraIssue): IJiraIssueStatus | undefined {
    let status: IJiraIssueStatus = issue.fields.status;

    if (isNullOrUndefined(issue.fields.subtasks)) {
      return status;
    }

    const issueState = issue.fields.subtasks.filter((subtask: IJiraIssue) => subtask.fields.summary.toLowerCase().indexOf('issue state') !== -1)[0];
    if (!isNullOrUndefined(issueState)) {
      status = issueState.fields.status;
    }

    return status;
  };


}
