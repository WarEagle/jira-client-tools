import * as JiraApi from "jira-client";
import IJiraIssueStatus = JiraApi.IJiraIssueStatus;
export declare module JiraClientTools {
    const CUSTOMFIELD_STORYPOINTS_ESTIMATE = "customfield_40892";
    const CUSTOMFIELD_STORYPOINTS_SPENT = "customfield_46095";
    const CUSTOMFIELD_SPRINTS = "customfield_41780";
    class IJiraIssueStatusName {
        readonly name: string;
        readonly order: number;
        static ALL: IJiraIssueStatusName[];
        static INCOMING: IJiraIssueStatusName;
        static OPEN: IJiraIssueStatusName;
        static REOPENED: IJiraIssueStatusName;
        static IN_PROGRESS: IJiraIssueStatusName;
        static REVIEW: IJiraIssueStatusName;
        static MERGE: IJiraIssueStatusName;
        static STABLE_REVIEW: IJiraIssueStatusName;
        static RESOLVED: IJiraIssueStatusName;
        static fromString(name: string): IJiraIssueStatusName;
        private constructor();
    }
    const extractAndParseDevSummaryJson: (inputString: string) => any;
    const extractStatus: (issue: any) => IJiraIssueStatus | undefined;
}
