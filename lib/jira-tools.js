"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JiraClientTools = void 0;
var util_1 = require("util");
var JiraClientTools;
(function (JiraClientTools) {
    JiraClientTools.CUSTOMFIELD_STORYPOINTS_ESTIMATE = 'customfield_40892';
    JiraClientTools.CUSTOMFIELD_STORYPOINTS_SPENT = 'customfield_46095';
    JiraClientTools.CUSTOMFIELD_SPRINTS = 'customfield_41780';
    var IJiraIssueStatusName = /** @class */ (function () {
        function IJiraIssueStatusName(name, order) {
            this.name = name;
            this.order = order;
            IJiraIssueStatusName.ALL.push(this);
        }
        IJiraIssueStatusName.fromString = function (name) {
            for (var _i = 0, _a = this.ALL; _i < _a.length; _i++) {
                var key = _a[_i];
                if (key.name === name) {
                    return key;
                }
            }
            console.error("Unknown status " + name);
            return undefined;
        };
        IJiraIssueStatusName.ALL = [];
        IJiraIssueStatusName.INCOMING = new IJiraIssueStatusName("Incoming", 0);
        IJiraIssueStatusName.OPEN = new IJiraIssueStatusName("Open", 1);
        IJiraIssueStatusName.REOPENED = new IJiraIssueStatusName("Reopened", 2);
        IJiraIssueStatusName.IN_PROGRESS = new IJiraIssueStatusName("In Progress", 3);
        IJiraIssueStatusName.REVIEW = new IJiraIssueStatusName("Review", 4);
        IJiraIssueStatusName.MERGE = new IJiraIssueStatusName("Merge", 5);
        IJiraIssueStatusName.STABLE_REVIEW = new IJiraIssueStatusName("Stable Review", 6);
        IJiraIssueStatusName.RESOLVED = new IJiraIssueStatusName("Resolved", 7);
        return IJiraIssueStatusName;
    }());
    JiraClientTools.IJiraIssueStatusName = IJiraIssueStatusName;
    JiraClientTools.extractAndParseDevSummaryJson = function (inputString) {
        var workingString = inputString.trim();
        var start = workingString.indexOf('devSummaryJson');
        if (start === -1) {
            return undefined;
        }
        var tmp = workingString.substr(start + "devSummaryJson".length + 1);
        tmp = tmp.substr(0, tmp.length - 1);
        // console.log(tmp);
        return JSON.parse(tmp);
    };
    JiraClientTools.extractStatus = function (issue) {
        var status = issue.fields.status;
        if (util_1.isNullOrUndefined(issue.fields.subtasks)) {
            return status;
        }
        var issueState = issue.fields.subtasks.filter(function (subtask) { return subtask.fields.summary.toLowerCase().indexOf('issue state') !== -1; })[0];
        if (!util_1.isNullOrUndefined(issueState)) {
            status = issueState.fields.status;
        }
        return status;
    };
})(JiraClientTools = exports.JiraClientTools || (exports.JiraClientTools = {}));
//# sourceMappingURL=jira-tools.js.map